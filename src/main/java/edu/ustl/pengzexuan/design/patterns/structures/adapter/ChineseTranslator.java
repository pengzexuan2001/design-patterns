package edu.ustl.pengzexuan.design.patterns.structures.adapter;

/**
 * 翻译器
 *
 * @author pengzexuan
 */
public class ChineseTranslator implements Translator{
    /**
     * 翻译
     *
     * @param content 原有的字幕
     * @return 翻译结果
     */
    @Override
    public String translate(String content) {
        if ("Hello, World".equals(content)) {
            return "你好，世界";
        } else {
            return "*************";
        }
    }
}
