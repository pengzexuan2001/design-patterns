# 工厂模式之——工厂方法模式（Factory Method Pattern）

## 角色
四个角色
Product：抽象产品
ConcreteProduct：具体产品
Factory：抽象工厂
ConcreteFactory：具体工厂


## 图示
<img src="../../../../images/creations/factory/factoryMethod/工厂方法示例.png" alt="图片加载失败">

## 存在问题
缺点：系统复杂度增加，产品单一