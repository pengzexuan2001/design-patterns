package edu.ustl.pengzexuan.design.patterns.creations.factory.abstractFactory.factory;

import edu.ustl.pengzexuan.design.patterns.creations.factory.abstractFactory.product.AbstractCar;
import edu.ustl.pengzexuan.design.patterns.creations.factory.abstractFactory.product.AbstractMask;
import edu.ustl.pengzexuan.design.patterns.creations.factory.abstractFactory.product.VanCar;

/**
 * 五菱货车厂
 *
 * @author pengzexuan
 */
public class WuLingVanCarFactory extends WuLingFactory{
    @Override
    public AbstractCar newCar() {
        return new VanCar();
    }

    @Override
    public AbstractMask newMask() {
        return null;
    }
}
