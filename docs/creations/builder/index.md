# 创建型模式之——创建者模式（Builder Pattern）
DATE：2022年2月17日

## 核心
产品角色（Product）：Phone
抽象建造者（Builder）：AbstracPhoneBuilder
具体建造者(Concrete Builder）：PhoneBuilder
创建的东西细节复杂，还必须暴露给使用者。屏蔽过程而不屏蔽细节


## 特点
- 屏蔽过程而不屏蔽细节

## 图示
<img src="../../../images/creations/builder/建造者模式示例.png" alt="图片加载失败">

## 应用场景什么场景用到？
- StringBuilder：append(); 给谁append呢？
- Swagger-ApiBuilder：
- 快速实现。Lombok-Builder模式
- .......




