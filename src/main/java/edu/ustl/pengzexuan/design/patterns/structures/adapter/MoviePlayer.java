package edu.ustl.pengzexuan.design.patterns.structures.adapter;

/**
 * 电影播放器
 *
 * @author pengzexuan
 */
public class MoviePlayer implements Player {
    @Override
    public String play() {
        System.err.println("正在播放XXXX");
        String content = "Hello, World";
        System.err.println(content);
        return content;
    }
}
