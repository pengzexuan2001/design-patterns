package edu.ustl.pengzexuan.design.patterns.structures.adapter.obj;

import edu.ustl.pengzexuan.design.patterns.structures.adapter.ChineseTranslator;
import edu.ustl.pengzexuan.design.patterns.structures.adapter.Player;
import edu.ustl.pengzexuan.design.patterns.structures.adapter.Translator;

/**
 * 组合的方式，通过类结构模型，将翻译器适配过来
 *
 * @author pengzexuan
 */
public class ChineseMovieAdapter extends ChineseTranslator implements Player {

    private final Translator translator = new ChineseTranslator();

    private final Player target;

    public ChineseMovieAdapter(Player target) {
        this.target = target;
    }

    @Override
    public String play() {
        String play = this.target.play();
        return this.translate(play);
    }
}
