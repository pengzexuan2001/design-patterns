package edu.ustl.pengzexuan.design.patterns.creations.singleton.entity;

import lombok.Data;

/**
 * 单例模式(饿汉式)测试实体类
 *
 * @author pengzexuan
 */
@Data
public class HungaryMan {

    private String name;
    private Integer age;

    /**
     * static person
     */
    private final static HungaryMan INSTANCE = new HungaryMan("pengzexuan", 20);

    /**
     * 私有化构造方法，保证无法进行new的操作，确保无法进行外部实例化
     */
    private HungaryMan(String name, Integer age) {
        this.name = name;
        this.age = age;
        System.err.println("调用了构造器");
    }

    /**
     * 暴露给外部的接口，创建单例的Person对象
     *
     * @return 构造出来的单例
     */
    public static HungaryMan hungaryMethod() {
        return INSTANCE;
    }
}
