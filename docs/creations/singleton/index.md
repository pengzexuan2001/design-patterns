# 创建型模式之——单例模式（singleton pattern）
DATE：2022年2月17日

## 核心
<div style="color: red">一个单一的类，负责创建自己的对象，同时确保系统中只有单个对象被创建。
</div>

## 特点
- 某个类只能有一个实例；（构造器私有）
- 它必须自行创建这个实例；（自己编写实例化逻辑）
- 它必须自行向整个系统提供这个实例；（对外提供实例化方法）

## 图示
<img src="../../../images/creations/singleton/单例模式示例.png" alt="图片加载失败">

## 应用场景
- 多线程中的线程池
- 数据库的连接池
- 系统环境信息
- 上下文（ServletContext）
- ......

## 常见问题
- 系统环境信息（System.getProperties()）？
- Spring中怎么保持组件单例的？
- ServletContext是什么（封装Servlet的信息）？是单例吗？怎么保证？
- ApplicationContext是什么？是单例吗？怎么保证？
- ApplicationContext： tomcat：一个应用（部署的一个war包）会有一个应用上下文
- ApplicationContext： Spring：表示整个IOC容器（怎么保证单例的）。ioc容器中有很多组件（怎么保证单例）
- 数据库连接池一般怎么创建出来的，怎么保证单实例？
- ......

//TODO: 关于内存可见性详情