package edu.ustl.pengzexuan.design.patterns.creations.factory.factoryMethod.factory;

import edu.ustl.pengzexuan.design.patterns.creations.factory.factoryMethod.product.AbstractCar;

/**
 * 抽象工厂
 *
 * @author pengzexuan
 */
public abstract class AbstractCarFactory {
    /**
     * 制造车
     *
     * @return 制造出来的车
     */
    public abstract AbstractCar newCar();
}
