package edu.ustl.pengzexuan.design.patterns.creations.factory.simpleFactory.factory;

import edu.ustl.pengzexuan.design.patterns.creations.factory.simpleFactory.constant.CarType;
import edu.ustl.pengzexuan.design.patterns.creations.factory.simpleFactory.product.AbstractCar;
import edu.ustl.pengzexuan.design.patterns.creations.factory.simpleFactory.product.MiniCar;
import edu.ustl.pengzexuan.design.patterns.creations.factory.simpleFactory.product.VanCar;

/**
 * 五菱汽车厂（简单工厂）
 * 产品数量有限
 *
 * @author pengzexuan
 */
public class WuLingSimpleFactory {

    /**
     * 制造汽车
     *
     * @param carType 汽车类型
     * @return 制造出来的汽车
     */
    public AbstractCar newCar(CarType carType) {
        return switch (carType) {
            case MINI_CAR -> new MiniCar();
            case VAN_CAR -> new VanCar();
        };
    }
}
