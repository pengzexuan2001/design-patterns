package edu.ustl.pengzexuan.design.patterns.creations.factory.factoryMethod.factory;

import edu.ustl.pengzexuan.design.patterns.creations.factory.factoryMethod.product.AbstractCar;
import edu.ustl.pengzexuan.design.patterns.creations.factory.factoryMethod.product.MiniCar;

/**
 * 五菱小汽车厂
 *
 * @author pengzexuan
 */
public class WuLingMiniCarFactory extends AbstractCarFactory {
    @Override
    public AbstractCar newCar() {
        return new MiniCar();
    }
}
