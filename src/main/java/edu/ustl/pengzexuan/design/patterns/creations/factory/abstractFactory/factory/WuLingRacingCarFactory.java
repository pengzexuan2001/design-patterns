package edu.ustl.pengzexuan.design.patterns.creations.factory.abstractFactory.factory;

import edu.ustl.pengzexuan.design.patterns.creations.factory.abstractFactory.product.AbstractCar;
import edu.ustl.pengzexuan.design.patterns.creations.factory.abstractFactory.product.AbstractMask;
import edu.ustl.pengzexuan.design.patterns.creations.factory.abstractFactory.product.RacingCar;

/**
 * 五菱赛车厂
 *
 * @author pengzexuan
 */
public class WuLingRacingCarFactory extends WuLingFactory{
    @Override
    public AbstractCar newCar() {
        return new RacingCar();
    }

    @Override
    public AbstractMask newMask() {
        return null;
    }
}
