# 工厂模式之——抽象工厂模式（Abstract Factory Pattern）

## 图示
<img src="../../../../images/creations/factory/abstractFactory/产品分层.png" alt="图片加载失败">
<img src="../../../../images/creations/factory/abstractFactory/抽象工厂示例.png" alt="图片加载失败">

## 存在问题
缺点：系统复杂度增加，产品单一