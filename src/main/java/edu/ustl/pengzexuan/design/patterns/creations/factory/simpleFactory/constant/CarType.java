package edu.ustl.pengzexuan.design.patterns.creations.factory.simpleFactory.constant;

/**
 * 汽车类型枚举
 *
 * @author pengzexuan
 */

public enum CarType {

    /**
     * 小汽车
     */
    MINI_CAR,

    /**
     * 货车
     */
    VAN_CAR;
}
