# 工厂模式之——简单工厂（Simple Factory Pattern）

## 角色
三个角色
Factory：工厂角色， WuLinFactory
Product：抽象产品角色，Car
ConcreteProduct：具体产品角色， VanCar、MiniCar

## 图示
<img src="../../../../images/creations/factory/simpleFactory/简单工厂示例.png" alt="图片加载失败">

## 存在问题
缺点：违背开闭，扩展不易