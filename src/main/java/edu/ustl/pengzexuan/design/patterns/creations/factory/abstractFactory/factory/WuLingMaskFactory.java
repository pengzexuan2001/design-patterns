package edu.ustl.pengzexuan.design.patterns.creations.factory.abstractFactory.factory;

import edu.ustl.pengzexuan.design.patterns.creations.factory.abstractFactory.product.AbstractCar;
import edu.ustl.pengzexuan.design.patterns.creations.factory.abstractFactory.product.AbstractMask;
import edu.ustl.pengzexuan.design.patterns.creations.factory.abstractFactory.product.N95Mask;

/**
 * 五菱口罩厂
 *
 * @author pengzexuan
 */
public class WuLingMaskFactory extends WuLingFactory{
    @Override
    public AbstractCar newCar() {
        return null;
    }

    @Override
    public AbstractMask newMask() {
        return new N95Mask();
    }
}
