package edu.ustl.pengzexuan.design.patterns.creations.prototype;

import edu.ustl.pengzexuan.design.patterns.creations.prototype.entity.User;
import edu.ustl.pengzexuan.design.patterns.creations.prototype.framework.MyMybatis;
import org.junit.Test;

/**
 * 原型模式主测试类
 * <p>
 * 适用于创建重复对象，同时保证性能
 * <div>1、MyMybatis：操纵数据库，从数据库里查出大量数据，（70~80%）很少变化</div>
 * 每次查询数据库，将查询结果封装，返回
 * <div>2、10000Thread，查询 new User("zhangSan", 18);每次都会创建一个对象封装返回，最终10000个User浪费内存</div>
 * <div>3、缓存解决，查询过的，进行缓存，保存其原型对象（再查相同的对象，直接拿出）</div>
 * <div style="color: red">4、直接获取缓存有严重的风险：直接获取缓存指针修改内存，会导致缓存中存在脏数据，为此切记调用clone</div>
 *
 * @author pengzexuan
 */
public class PrototypePatternTest {

    @Test
    public void test() {
        MyMybatis myMybatis = new MyMybatis();
        System.err.println(myMybatis.getUser("zhangSan"));
        System.err.println(myMybatis.getUser("zhangSan"));
        System.err.println(myMybatis.getUser("zhangSan"));
        User zhangSan = myMybatis.getUser("zhangSan");
        zhangSan.setUsername("liSi");
        System.err.println(zhangSan);
        System.err.println(myMybatis.getUser("zhangSan"));
        System.err.println(myMybatis.getUser("zhangSan"));
        System.err.println(myMybatis.getUser("zhangSan"));
        System.err.println(myMybatis.getUser("zhangSan"));
        System.err.println(myMybatis.getUser("zhangSan"));
        System.err.println(myMybatis.getUser("zhangSan"));
    }
}
