package edu.ustl.pengzexuan.design.patterns.creations.singleton.entity;

import lombok.Data;

/**
 * 单例模式(懒汉式)测试实体类
 * <div>
 *     <b>注意多线程问题！！！</b>
 * </div>
 *
 * @author pengzexuan
 */
@Data
public class LazyMan {

    private String name;
    private Integer age;

    /**
     * static instance
     * <b>加内存可见性!</b>
     */
    private static volatile LazyMan instance;

    /**
     * 私有化构造方法，保证无法进行new的操作，确保无法进行外部实例化
     */
    private LazyMan() {
        System.err.println("调用了构造器");
    }

    /**
     * 暴露给外部的接口，创建单例的Person对象
     * 考虑多线程问题，假设
     * <div>public static synchronized LazyMan lazyMethod() 锁🔐太大</div>
     * <b>双重检查锁🔐</b>
     *
     * @return instance
     */
    public static synchronized LazyMan lazyMethod() {
        // 如果instance未有分配
        if (instance == null) {
            synchronized (LazyMan.class) {
                if (instance == null) {
                    instance = new LazyMan();
                    instance.setName("pengzexuan");
                    instance.setAge(18);
                }
            }
        }
        return instance;
    }
}
