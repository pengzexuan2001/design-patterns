package edu.ustl.pengzexuan.design.patterns.creations.factory.simpleFactory.product;

/**
 * 货车
 *
 * @author pengzexuan
 */
public class VanCar extends AbstractCar {

    public VanCar() {
        this.engine = "单缸柴油机";
    }

    @Override
    public void run() {
        System.err.println(this.engine + "---->" + "哒哒哒……");
    }
}
