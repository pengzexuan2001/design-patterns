package edu.ustl.pengzexuan.design.patterns.creations.factory.abstractFactory.product;

/**
 * 赛车
 *
 * @author pengzexuan
 */
public class RacingCar extends AbstractCar {

    public RacingCar() {
        this.engine = "v8发动机";
    }

    @Override
    public void run() {
        System.err.println(this.engine + "---->" + "嗖。。。。。。。。。");
    }
}
