package edu.ustl.pengzexuan.design.patterns.creations.prototype.entity;

import lombok.Data;

/**
 * 测试用实体类——User
 *
 * @author pengzexuan
 */
@Data
public class User implements Cloneable {
    private String username;
    private Integer age;

    public User(String username, Integer age) {
        this.username = username;
        this.age = age;
        System.err.println("创建了名称为：" + username + "的对象");
    }

    @Override
    public User clone() {
        try {
            return (User) super.clone();
        } catch (CloneNotSupportedException e) {
            throw new AssertionError();
        }
    }
}
