package edu.ustl.pengzexuan.design.patterns.structures.adapter;

import edu.ustl.pengzexuan.design.patterns.structures.adapter.clazz.ChineseMovieAdapter;
import org.junit.Test;

/**
 * 适配器模式测试类
 *
 * @author pengzexuan
 */
public class AdapterTest {
    @Test
    public void test() {
        ChineseMovieAdapter chineseMovieAdapter = new ChineseMovieAdapter(new MoviePlayer());
        System.err.println(chineseMovieAdapter.play());
    }
}
