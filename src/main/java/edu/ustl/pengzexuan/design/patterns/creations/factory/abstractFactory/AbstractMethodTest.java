package edu.ustl.pengzexuan.design.patterns.creations.factory.abstractFactory;

import edu.ustl.pengzexuan.design.patterns.creations.factory.abstractFactory.factory.*;
import edu.ustl.pengzexuan.design.patterns.creations.factory.abstractFactory.product.AbstractCar;
import edu.ustl.pengzexuan.design.patterns.creations.factory.abstractFactory.product.AbstractMask;
import org.junit.Test;

/**
 * 抽象工厂测试类
 *
 * @author pengzexuan
 */
public class AbstractMethodTest {
    @Test
    public void test() {
        WuLingFactory wuLingFactory = new WuLingMaskFactory();
        AbstractMask n95Mask = wuLingFactory.newMask();
        n95Mask.protectMe();
        // 。。。。。
        wuLingFactory = new WuLingHangZhouFactory();
        AbstractMask normalMask = wuLingFactory.newMask();
        normalMask.protectMe();
        wuLingFactory = new WuLingVanCarFactory();
        AbstractCar vanCar = wuLingFactory.newCar();
        vanCar.run();
        wuLingFactory = new WuLingRacingCarFactory();
        AbstractCar racingCar = wuLingFactory.newCar();
        racingCar.run();
    }
}
