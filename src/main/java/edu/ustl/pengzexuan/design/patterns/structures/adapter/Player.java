package edu.ustl.pengzexuan.design.patterns.structures.adapter;

/**
 * 系统原有接口player——可以播放电影，返回字幕
 *
 * @author pengzexuan
 */
public interface Player {
    /**
     * 播放电影，返回字幕
     *
     * @return 字幕
     */
    String play();
}
