package edu.ustl.pengzexuan.design.patterns.creations.factory.abstractFactory.product;

import java.math.BigDecimal;

/**
 * N95口罩
 *
 * @author pengzexuan
 */
public class N95Mask extends AbstractMask{

    public N95Mask() {
        this.price = new BigDecimal(15);
    }

    @Override
    public void protectMe() {
        System.err.println("N95口罩，超级防护===>" + this.price + "$");
    }
}
