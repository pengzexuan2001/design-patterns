package edu.ustl.pengzexuan.design.patterns.creations.prototype.framework;

import edu.ustl.pengzexuan.design.patterns.creations.prototype.entity.User;

import java.util.HashMap;
import java.util.Map;

/**
 * 模拟Mybatis缓存
 *
 * @author pengzexuan
 */
public class MyMybatis {

    /**
     * 缓存
     */
    private final Map<String, User> cache = new HashMap<>();

    /**
     * 模拟从数据库中查询用户
     *
     * @param username 用户名
     * @return 所查到的用户
     */
    private User getUserFromDataBase(String username) {
        // 从数据库查
        System.err.println("从数据库查询了" + username);
        User user = new User(username, 18);
        // 写缓存也要写clone对象
        this.cache.put(username, user.clone());
        System.err.println("录入缓存" + user);
        return user;
    }

    /**
     * 模拟查询
     *
     * @param username 用户名
     * @return 所查到的用户
     */
    public User getUser(String username) {
        // 缓存未命中
        if (!this.cache.containsKey(username)) {
            System.err.println("缓存未命中");
            return this.getUserFromDataBase(username);
        } else {
            System.err.println("缓存命中");
            // 谨防脏缓存问题
            // return this.cache.get(username) ❌
            return this.cache.get(username).clone();
        }
    }
}
