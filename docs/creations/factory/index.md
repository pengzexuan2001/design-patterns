# 创建型模式之——工厂模式（Factory Pattern）

DATA：2022年2月17日

工厂模式（Factory Pattern）提供了一种创建对象的最佳方式。我们不必关心对象的创建细节，只需要根据不同情况获取不同产品即可。难点：写好我们的工厂
- 简单工厂(Simple Factory 静态工厂)
- 工厂方法(Factory Method 多态工厂)
- 抽象工厂(Abstract Factory)

## 工厂的退化
当抽象工厂模式中每一个具体工厂类只创建一个产品对象，也就是只存在一个产品等级结构时，抽象工厂模式退化成工厂方法模式；当工厂方法模式中抽象工厂与具体工厂合并，提供一个统一的工厂来创建产品对象，并将创建对象的工厂方法设计为静态方法时，工厂方法模式退化成简单工厂模式。

## 应用场景
- NumberFormat、SimpleDateFormat
- LoggerFactory：
- SqlSessionFactory：MyBatis
- BeanFactory：Spring的BeanFactory（就是为了造出bean）
- ......