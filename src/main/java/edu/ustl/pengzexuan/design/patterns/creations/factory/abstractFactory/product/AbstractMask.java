package edu.ustl.pengzexuan.design.patterns.creations.factory.abstractFactory.product;

import java.math.BigDecimal;

/**
 * 抽象口罩
 * @author pengzexuan
 */
public abstract class AbstractMask {
    BigDecimal price;

    /**
     * 保护我
     */
    public abstract void protectMe();
}
