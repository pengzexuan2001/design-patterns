package edu.ustl.pengzexuan.design.patterns.creations.factory.simpleFactory;

import edu.ustl.pengzexuan.design.patterns.creations.factory.simpleFactory.constant.CarType;
import edu.ustl.pengzexuan.design.patterns.creations.factory.simpleFactory.factory.WuLingSimpleFactory;
import edu.ustl.pengzexuan.design.patterns.creations.factory.simpleFactory.product.AbstractCar;
import org.junit.Test;

/**
 * 简单工厂测试类
 *
 * @author pengzexuan
 */
public class SimpleFactoryTest {
    @Test
    public void test() {
        WuLingSimpleFactory wuLingSimpleFactory = new WuLingSimpleFactory();
        AbstractCar vanCar = wuLingSimpleFactory.newCar(CarType.VAN_CAR);
        AbstractCar miniCar = wuLingSimpleFactory.newCar(CarType.MINI_CAR);
        System.err.println(vanCar);
        System.err.println(miniCar);
        vanCar.run();
        miniCar.run();
    }
}
