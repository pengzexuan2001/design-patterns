package edu.ustl.pengzexuan.design.patterns.creations.factory.abstractFactory.factory;

import edu.ustl.pengzexuan.design.patterns.creations.factory.abstractFactory.product.AbstractCar;
import edu.ustl.pengzexuan.design.patterns.creations.factory.abstractFactory.product.AbstractMask;
import edu.ustl.pengzexuan.design.patterns.creations.factory.abstractFactory.product.CommonMask;

/**
 * 五菱杭州口罩厂
 *
 * @author pengzexuan
 */
public class WuLingHangZhouFactory extends WuLingFactory{
    @Override
    public AbstractCar newCar() {
        return null;
    }

    @Override
    public AbstractMask newMask() {
        return new CommonMask();
    }
}
