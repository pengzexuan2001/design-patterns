package edu.ustl.pengzexuan.design.patterns.creations.factory.abstractFactory.factory;

import edu.ustl.pengzexuan.design.patterns.creations.factory.abstractFactory.product.AbstractCar;
import edu.ustl.pengzexuan.design.patterns.creations.factory.abstractFactory.product.AbstractMask;

/**
 * 五菱工厂--->总厂，定义能干的活
 *
 * @author pengzexuan
 */
public abstract class WuLingFactory {
    /**
     * 造车
     *
     * @return 车
     */
    public abstract AbstractCar newCar();

    /**
     * 造口罩
     *
     * @return 口罩
     */
    public abstract AbstractMask newMask();
}
