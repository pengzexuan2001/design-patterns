package edu.ustl.pengzexuan.design.patterns.creations.singleton;

import edu.ustl.pengzexuan.design.patterns.creations.singleton.entity.HungaryMan;
import edu.ustl.pengzexuan.design.patterns.creations.singleton.entity.LazyMan;
import org.junit.Test;

/**
 * 单例模式主测试类
 *
 * @author pengzexuan
 */
public class SingletonPatternTest {
    /**
     * 懒汉式单例
     */
    @Test
    public void testLazy() {
        LazyMan person1 = LazyMan.lazyMethod();
        LazyMan person2 = LazyMan.lazyMethod();
        // person1 与 person2 的指向应当是一致的
        System.err.println(person1 == person2);
    }

    @Test
    public void testHungary() {
        HungaryMan person1 = HungaryMan.hungaryMethod();
        HungaryMan person2 = HungaryMan.hungaryMethod();
        // person1 与 person2 的指向应当是一致的
        System.err.println(person1 == person2);
    }
}
