package edu.ustl.pengzexuan.design.patterns.structures.adapter.clazz;

import edu.ustl.pengzexuan.design.patterns.structures.adapter.ChineseTranslator;
import edu.ustl.pengzexuan.design.patterns.structures.adapter.Player;

/**
 * 继承的方式，通过类结构模型，将翻译器适配过来
 *
 * @author pengzexuan
 */
public class ChineseMovieAdapter extends ChineseTranslator implements Player {

    private final Player target;

    public ChineseMovieAdapter(Player target) {
        this.target = target;
    }

    @Override
    public String play() {
        String play = this.target.play();
        return this.translate(play);
    }
}
