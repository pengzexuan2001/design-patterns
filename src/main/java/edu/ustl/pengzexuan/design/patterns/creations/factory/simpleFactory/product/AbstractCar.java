package edu.ustl.pengzexuan.design.patterns.creations.factory.simpleFactory.product;

/**
 * 简单工厂的产品——Car
 *
 * @author pengzexuan
 */
public abstract class AbstractCar {
    String engine;

    /**
     * 车辆运行
     */
    public abstract void run();
}
