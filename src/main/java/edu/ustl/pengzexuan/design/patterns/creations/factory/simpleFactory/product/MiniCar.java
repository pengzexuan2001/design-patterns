package edu.ustl.pengzexuan.design.patterns.creations.factory.simpleFactory.product;

/**
 * 小汽车
 *
 * @author pengzexuan
 */
public class MiniCar extends AbstractCar {

    public MiniCar() {
        this.engine = "四缸水平对置发动机";
    }

    @Override
    public void run() {
        System.err.println(this.engine + "---->" + "嘟嘟嘟");
    }
}
