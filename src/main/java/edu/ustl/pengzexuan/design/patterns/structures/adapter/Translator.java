package edu.ustl.pengzexuan.design.patterns.structures.adapter;

/**
 * 系统原有接口，可以翻译字幕内容
 *
 * @author pengzexuan
 */
public interface Translator {
    /**
     * 翻译字幕
     *
     * @param content 原有的字幕
     * @return 翻译后的字幕
     */
    String translate(String content);
}
