package edu.ustl.pengzexuan.design.patterns.creations.factory.abstractFactory.product;

import java.math.BigDecimal;

/**
 * 普通口罩
 *
 * @author pengzexuan
 */
public class CommonMask extends AbstractMask{

    public CommonMask() {
        this.price = new BigDecimal(1);
    }

    @Override
    public void protectMe() {
        System.err.println("普通口罩简单保护===>" + this.price + "$");
    }
}
