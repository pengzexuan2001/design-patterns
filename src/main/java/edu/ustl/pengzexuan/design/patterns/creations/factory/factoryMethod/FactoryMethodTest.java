package edu.ustl.pengzexuan.design.patterns.creations.factory.factoryMethod;

import edu.ustl.pengzexuan.design.patterns.creations.factory.factoryMethod.factory.AbstractCarFactory;
import edu.ustl.pengzexuan.design.patterns.creations.factory.factoryMethod.factory.WuLingMiniCarFactory;
import edu.ustl.pengzexuan.design.patterns.creations.factory.factoryMethod.factory.WuLingRacingCarFactory;
import edu.ustl.pengzexuan.design.patterns.creations.factory.factoryMethod.factory.WuLingVanCarFactory;
import edu.ustl.pengzexuan.design.patterns.creations.factory.factoryMethod.product.AbstractCar;
import org.junit.Test;

/**
 * 工厂方法测试类
 *
 * @author pengzexuan
 */
public class FactoryMethodTest {
    @Test
    public void test() {
        AbstractCarFactory abstractCarFactory = new WuLingMiniCarFactory();
        AbstractCar miniCar = abstractCarFactory.newCar();
        System.err.println(miniCar);
        miniCar.run();
        abstractCarFactory = new WuLingVanCarFactory();
        AbstractCar vanCar = abstractCarFactory.newCar();
        System.err.println(vanCar);
        vanCar.run();
        abstractCarFactory = new WuLingRacingCarFactory();
        AbstractCar racingCar = abstractCarFactory.newCar();
        System.err.println(racingCar);
        racingCar.run();
    }
}
