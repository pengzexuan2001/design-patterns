package edu.ustl.pengzexuan.design.patterns.creations.factory.factoryMethod.factory;

import edu.ustl.pengzexuan.design.patterns.creations.factory.factoryMethod.product.AbstractCar;
import edu.ustl.pengzexuan.design.patterns.creations.factory.factoryMethod.product.RacingCar;

/**
 * 五菱赛车厂
 *
 * @author pengzexuan
 */
public class WuLingRacingCarFactory extends AbstractCarFactory {
    @Override
    public AbstractCar newCar() {
        return new RacingCar();
    }
}
