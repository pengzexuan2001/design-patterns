package edu.ustl.pengzexuan.design.patterns.creations.factory.factoryMethod.factory;

import edu.ustl.pengzexuan.design.patterns.creations.factory.factoryMethod.product.AbstractCar;
import edu.ustl.pengzexuan.design.patterns.creations.factory.factoryMethod.product.VanCar;

/**
 * 五菱货车厂
 *
 * @author pengzexuan
 */
public class WuLingVanCarFactory extends AbstractCarFactory{
    @Override
    public AbstractCar newCar() {
        return new VanCar();
    }
}
